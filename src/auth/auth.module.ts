import { AuthService } from './shared/auth.service';
import { Module } from '@nestjs/common';
import { UsuariosModule } from '../domain/usuarios/usuarios.module';
import { LocalStrategy } from './shared/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './shared/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './shared/constants';

@Module({
  imports: [
    UsuariosModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
    }),
  ],
  controllers: [],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
