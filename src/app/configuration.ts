import * as dotenv from 'dotenv';

dotenv.config();

const database = {
  type: process.env.DB_TYPE as any,
  host: process.env.DB_HOST,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DATABASE_NAME,
};

for (const cfg in database) {
  if (!database[cfg]) {
    delete database[cfg];
  }
}

export default {
  database: database,
};
