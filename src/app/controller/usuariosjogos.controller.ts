import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/shared/jw-auth.guard';
import { UsuariosJogosDTO } from '../../domain/usuariosjogos/dto/usuariosjogos.dto';
import { UsuariosJogosService } from '../../domain/usuariosjogos/service/usuariosjogos.service';

@Controller('/usuarios')
export class UsuariosJogosController {
  constructor(readonly service: UsuariosJogosService) {}

  @UseGuards(JwtAuthGuard)
  @Post('vincular')
  async create(@Body() usuariosjogos: UsuariosJogosDTO): Promise<any> {
    return this.service.create(usuariosjogos);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('desvincular/:id')
  async delete(@Param('id') id: number): Promise<any> {
    return this.service.delete(id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('listarVinculos')
  async findAndCount(): Promise<[UsuariosJogosDTO[], number]> {
    return this.service.findAndCount();
  }

  @UseGuards(JwtAuthGuard)
  @Get('listarMelhoresJogos')
  async listarMelhoresJogos() {
    return this.service.listarJogosQuantidade(true);
  }

  @UseGuards(JwtAuthGuard)
  @Get('listarJogosQuantidade')
  async listarJogosQuantidade() {
    return this.service.listarJogosQuantidade();
  }

  @UseGuards(JwtAuthGuard)
  @Get('recuperarVinculo/:id')
  async findOneByID(@Param('id') id: number): Promise<UsuariosJogosDTO> {
    return this.service.findOneByID(id);
  }
}
