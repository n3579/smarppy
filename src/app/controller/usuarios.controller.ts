import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/shared/jw-auth.guard';
import { UsuariosDTO } from '../../domain/usuarios/dto/usuariosDTO.dto';
import { UsuariosService } from '../../domain/usuarios/service/usuarios.service';

@Controller('/usuarios')
export class UsuariosController {
  constructor(readonly service: UsuariosService) {}

  @Post('cadastrar')
  async create(@Body() usuarios: UsuariosDTO): Promise<any> {
    return this.service.create(usuarios);
  }

  @UseGuards(JwtAuthGuard)
  @Put('editar/:id/')
  async update(
    @Param('id') id: number,
    @Body() usuarios: UsuariosDTO,
  ): Promise<any> {
    usuarios.id = id;
    return this.service.update(usuarios);
  }

  @UseGuards(JwtAuthGuard)
  @Get('listar')
  async findAndCount(): Promise<[UsuariosDTO[], number]> {
    return this.service.findAndCount();
  }

  @UseGuards(JwtAuthGuard)
  @Get('recuperar/:id')
  async findOneByID(@Param('id') id: number): Promise<UsuariosDTO> {
    return this.service.findOneByID(id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('deletar/:id')
  async delete(@Param('id') id: number): Promise<any> {
    return this.service.delete(id);
  }
}
