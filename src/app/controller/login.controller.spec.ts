import { Test } from '@nestjs/testing/test';
import { AuthService } from '../../auth/shared/auth.service';
import { LoginController } from './login.controller';
describe('LoginController', () => {
  let loginController: LoginController;

  const authServiceMock = {
    login: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [{ provide: AuthService, useValue: authServiceMock }],
      controllers: [LoginController],
    }).compile();

    loginController = module.get<LoginController>(LoginController);
  });

  it('UsuariosController está definido', () => {
    expect(loginController).toBeDefined();
  });

  it('login() - testando a rota auth/login', async () => {
    const result = await loginController.login({});

    expect(authServiceMock.login).toBeCalledTimes(1);
  });
});
