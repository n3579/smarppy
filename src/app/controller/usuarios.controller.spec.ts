import { Test } from '@nestjs/testing/test';
import { UsuariosService } from '../../domain/usuarios/service/usuarios.service';
import { UsuariosController } from './usuarios.controller';
describe('UsuariosController', () => {
  let usuariosController: UsuariosController;

  const usuariosServiceMock = {
    create: jest.fn(),
    update: jest.fn(),
    findAndCount: jest.fn(),
    findOneByID: jest.fn(),
    delete: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [{ provide: UsuariosService, useValue: usuariosServiceMock }],
      controllers: [UsuariosController],
    }).compile();

    usuariosController = module.get<UsuariosController>(UsuariosController);
  });

  it('UsuariosController está definido', () => {
    expect(usuariosController).toBeDefined();
  });

  it('create() - testando a rota cadastrar', async () => {
    const result = await usuariosController.create({ UserName: 'test' } as any);

    expect(usuariosServiceMock.create).toBeCalledTimes(1);
    expect(usuariosServiceMock.create).toBeCalledWith({
      UserName: 'test',
    } as any);
  });

  it('update() - testando a rota editar', async () => {
    const result = await usuariosController.update(1, {} as any);

    expect(usuariosServiceMock.update).toBeCalledTimes(1);
    expect(usuariosServiceMock.update).toBeCalledWith({ id: 1 });
  });

  it('findAndCount() - testando a rota listar', async () => {
    const result = await usuariosController.findAndCount();

    expect(usuariosServiceMock.findAndCount).toBeCalledTimes(1);
    expect(usuariosServiceMock.findAndCount).toBeCalledWith();
  });

  it('findOneByID() - testando a rota recuperar/:id', async () => {
    const result = await usuariosController.findOneByID(1);

    expect(usuariosServiceMock.findOneByID).toBeCalledTimes(1);
    expect(usuariosServiceMock.findOneByID).toBeCalledWith(1);
  });

  it('delete() - testando a rota deletar/:id', async () => {
    const result = await usuariosController.delete(1);

    expect(usuariosServiceMock.delete).toBeCalledTimes(1);
    expect(usuariosServiceMock.delete).toBeCalledWith(1);
  });
});
