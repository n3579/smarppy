import { Test } from '@nestjs/testing/test';
import { CategoriaService } from '../../domain/categoria/service/categoria.service';
import { CategoriaController } from './categoria.controller';

describe('CategoriaController', () => {
  let categoriaController: CategoriaController;

  const categoriaServiceMock = {
    create: jest.fn(),
    update: jest.fn(),
    findAndCount: jest.fn(),
    findOneByID: jest.fn(),
    delete: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [
        { provide: CategoriaService, useValue: categoriaServiceMock },
      ],
      controllers: [CategoriaController],
    }).compile();

    categoriaController = module.get<CategoriaController>(CategoriaController);
  });

  it('CategoriaController está definido', () => {
    expect(categoriaController).toBeDefined();
  });

  it('create() - testando a rota cadastrar', async () => {
    const result = await categoriaController.create({
      UserName: 'test',
    } as any);

    expect(categoriaServiceMock.create).toBeCalledTimes(1);
    expect(categoriaServiceMock.create).toBeCalledWith({
      UserName: 'test',
    } as any);
  });

  it('update() - testando a rota editar', async () => {
    const result = await categoriaController.update(1, {} as any);

    expect(categoriaServiceMock.update).toBeCalledTimes(1);
    expect(categoriaServiceMock.update).toBeCalledWith({ id: 1 });
  });

  it('findAndCount() - testando a rota listar', async () => {
    const result = await categoriaController.findAndCount();

    expect(categoriaServiceMock.findAndCount).toBeCalledTimes(1);
    expect(categoriaServiceMock.findAndCount).toBeCalledWith();
  });

  it('findOneByID() - testando a rota recuperar/:id', async () => {
    const result = await categoriaController.findOneByID(1);

    expect(categoriaServiceMock.findOneByID).toBeCalledTimes(1);
    expect(categoriaServiceMock.findOneByID).toBeCalledWith(1);
  });

  it('delete() - testando a rota deletar/:id', async () => {
    const result = await categoriaController.delete(1);

    expect(categoriaServiceMock.delete).toBeCalledTimes(1);
    expect(categoriaServiceMock.delete).toBeCalledWith(1);
  });
});
