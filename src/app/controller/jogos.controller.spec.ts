import { Test } from '@nestjs/testing/test';
import { JogosService } from '../../domain/jogos/service/jogos.service';
import { JogosController } from './jogos.controller';
describe('JogosController', () => {
  let jogosController: JogosController;

  const jogosServiceMock = {
    create: jest.fn(),
    update: jest.fn(),
    findAndCount: jest.fn(),
    findOneByID: jest.fn(),
    delete: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [{ provide: JogosService, useValue: jogosServiceMock }],
      controllers: [JogosController],
    }).compile();

    jogosController = module.get<JogosController>(JogosController);
  });

  it('JogosController está definido', () => {
    expect(jogosController).toBeDefined();
  });

  it('create() - testando a rota cadastrar', async () => {
    const result = await jogosController.create({ UserName: 'test' } as any);

    expect(jogosServiceMock.create).toBeCalledTimes(1);
    expect(jogosServiceMock.create).toBeCalledWith({ UserName: 'test' } as any);
  });

  it('update() - testando a rota editar', async () => {
    const result = await jogosController.update(1, {} as any);

    expect(jogosServiceMock.update).toBeCalledTimes(1);
    expect(jogosServiceMock.update).toBeCalledWith({ id: 1 });
  });

  it('findAndCount() - testando a rota listar', async () => {
    const result = await jogosController.findAndCount();

    expect(jogosServiceMock.findAndCount).toBeCalledTimes(1);
    expect(jogosServiceMock.findAndCount).toBeCalledWith();
  });

  it('findOneByID() - testando a rota recuperar/:id', async () => {
    const result = await jogosController.findOneByID(1);

    expect(jogosServiceMock.findOneByID).toBeCalledTimes(1);
    expect(jogosServiceMock.findOneByID).toBeCalledWith(1);
  });

  it('delete() - testando a rota deletar/:id', async () => {
    const result = await jogosController.delete(1);

    expect(jogosServiceMock.delete).toBeCalledTimes(1);
    expect(jogosServiceMock.delete).toBeCalledWith(1);
  });
});
