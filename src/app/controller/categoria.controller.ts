import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/shared/jw-auth.guard';
import { CategoriaDTO } from '../../domain/categoria/dto/categoriaDTO.dto';
import { CategoriaService } from '../../domain/categoria/service/categoria.service';

@Controller('/categoria')
export class CategoriaController {
  constructor(readonly service: CategoriaService) {}

  @UseGuards(JwtAuthGuard)
  @Post('cadastrar')
  async create(@Body() categoria: CategoriaDTO): Promise<any> {
    return this.service.create(categoria);
  }

  @UseGuards(JwtAuthGuard)
  @Put('editar/:id/')
  async update(
    @Param('id') id: number,
    @Body() categoria: CategoriaDTO,
  ): Promise<any> {
    categoria.id = id;
    return this.service.update(categoria);
  }

  @UseGuards(JwtAuthGuard)
  @UseGuards(JwtAuthGuard)
  @Get('listar')
  async findAndCount(): Promise<[CategoriaDTO[], number]> {
    return this.service.findAndCount();
  }

  @UseGuards(JwtAuthGuard)
  @Get('recuperar/:id')
  async findOneByID(@Param('id') id: number): Promise<CategoriaDTO> {
    return this.service.findOneByID(id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('deletar/:id')
  async delete(@Param('id') id: number): Promise<any> {
    return this.service.delete(id);
  }
}
