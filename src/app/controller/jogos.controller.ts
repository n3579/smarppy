import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/shared/jw-auth.guard';
import { JogosDTO } from '../../domain/jogos/dto/jogosDTO.dto';
import { JogosService } from '../../domain/jogos/service/jogos.service';

@Controller('/jogos')
export class JogosController {
  constructor(readonly service: JogosService) {}

  @UseGuards(JwtAuthGuard)
  @Post('cadastrar')
  async create(@Body() jogos: JogosDTO): Promise<any> {
    return this.service.create(jogos);
  }

  @UseGuards(JwtAuthGuard)
  @Put('editar/:id/')
  async update(@Param('id') id: number, @Body() jogos: JogosDTO): Promise<any> {
    jogos.id = id;
    return this.service.update(jogos);
  }

  @UseGuards(JwtAuthGuard)
  @Get('listar')
  async findAndCount(): Promise<[JogosDTO[], number]> {
    return this.service.findAndCount();
  }

  @UseGuards(JwtAuthGuard)
  @Get('recuperar/:id')
  async findOneByID(@Param('id') id: number): Promise<JogosDTO> {
    return this.service.findOneByID(id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('deletar/:id')
  async delete(@Param('id') id: number): Promise<any> {
    return this.service.delete(id);
  }
}
