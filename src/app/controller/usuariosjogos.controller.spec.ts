import { Test } from '@nestjs/testing';
import { UsuariosJogosService } from '../../domain/usuariosjogos/service/usuariosjogos.service';
import { UsuariosJogosController } from './usuariosjogos.controller';

describe('UsuariosJogosController', () => {
  let usuariosJogosController: UsuariosJogosController;

  const usuariosJogosServiceMock = {
    create: jest.fn(),
    update: jest.fn(),
    findAndCount: jest.fn(),
    findOneByID: jest.fn(),
    delete: jest.fn(),
    listarJogosQuantidade: jest.fn(),
  };

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      providers: [
        { provide: UsuariosJogosService, useValue: usuariosJogosServiceMock },
      ],
      controllers: [UsuariosJogosController],
    }).compile();

    usuariosJogosController = module.get<UsuariosJogosController>(
      UsuariosJogosController,
    );
  });

  afterEach(() => {
    usuariosJogosServiceMock.create.mockClear();
    usuariosJogosServiceMock.update.mockClear();
    usuariosJogosServiceMock.findAndCount.mockClear();
    usuariosJogosServiceMock.findOneByID.mockClear();
    usuariosJogosServiceMock.listarJogosQuantidade.mockClear();
  });

  it('UsuariosController está definido', () => {
    expect(usuariosJogosController).toBeDefined();
  });

  it('create() - testando a rota vincular', async () => {
    const result = await usuariosJogosController.create({
      UserName: 'test',
    } as any);

    expect(usuariosJogosServiceMock.create).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.create).toBeCalledWith({
      UserName: 'test',
    } as any);
  });

  it('delete() - testando a rota desvincular/:id', async () => {
    const result = await usuariosJogosController.delete(1);

    expect(usuariosJogosServiceMock.delete).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.delete).toBeCalledWith(1);
  });

  it('findAndCount() - testando a rota listarVinculos', async () => {
    const result = await usuariosJogosController.findAndCount();

    expect(usuariosJogosServiceMock.findAndCount).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.findAndCount).toBeCalledWith();
  });

  it('listarJogosQuantidade() - testando a rota listarJogosQuantidade', async () => {
    const result = await usuariosJogosController.listarJogosQuantidade();

    expect(usuariosJogosServiceMock.listarJogosQuantidade).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.listarJogosQuantidade).toBeCalledWith();
  });

  it('listarMelhoresJogos() - testando a rota listarMelhoresJogos', async () => {
    const result = await usuariosJogosController.listarMelhoresJogos();

    expect(usuariosJogosServiceMock.listarJogosQuantidade).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.listarJogosQuantidade).toBeCalledWith(true);
  });

  it('findOneByID() - testando a rota recuperarVinculo/:id', async () => {
    const result = await usuariosJogosController.findOneByID(1);

    expect(usuariosJogosServiceMock.findOneByID).toBeCalledTimes(1);
    expect(usuariosJogosServiceMock.findOneByID).toBeCalledWith(1);
  });
});
