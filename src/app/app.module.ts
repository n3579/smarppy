import { LoginController } from './controller/login.controller';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Categoria } from '../domain/categoria/entity/categoria.entity';
import { DomainModule } from '../domain/domain.module';
import { Jogos } from '../domain/jogos/entity/jogos.entity';
import { Usuarios } from '../domain/usuarios/entity/usuarios.entity';
import { UsuariosJogos } from '../domain/usuariosjogos/entity/usuariosjogos.entity';
import configuration from './configuration';
import { CategoriaController } from './controller/categoria.controller';
import { JogosController } from './controller/jogos.controller';
import { UsuariosController } from './controller/usuarios.controller';
import { UsuariosJogosController } from './controller/usuariosjogos.controller';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forRoot({
      ...configuration.database,
      entities: [Categoria, Jogos, Usuarios, UsuariosJogos],
      logging: true,
      synchronize: true,
    }),
    DomainModule,
  ],
  controllers: [
    LoginController,
    CategoriaController,
    JogosController,
    UsuariosController,
    UsuariosJogosController,
  ],
  providers: [],
})
export class AppModule {}
