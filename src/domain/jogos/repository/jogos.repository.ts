import { EntityRepository, Repository } from 'typeorm';
import { Jogos } from '../entity/jogos.entity';

@EntityRepository(Jogos)
export class JogosRepository extends Repository<Jogos> {}
