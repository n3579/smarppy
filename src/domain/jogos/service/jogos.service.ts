import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CategoriaService } from '../../categoria/service/categoria.service';
import { JogosConverter } from '../converter/jogos.converter';
import { JogosDTO } from '../dto/jogosDTO.dto';
import { JogosRepository } from '../repository/jogos.repository';

@Injectable()
export class JogosService {
  constructor(
    @InjectRepository(JogosRepository)
    private repository: JogosRepository,
    private categoriaService: CategoriaService,
    private converter: JogosConverter,
  ) {}

  async create(jogos: JogosDTO): Promise<JogosDTO> {
    if (jogos.descricao.length === 0) {
      throw new BadRequestException('Descrição não pode ser vazia!');
    }

    if (jogos.id === 0) {
      throw new BadRequestException('ID não pode se zero!');
    }

    await this.categoriaService.findOneByID(jogos.idCategoria);

    const findJogos = await this.repository.findOne({
      where: { descricao: jogos.descricao },
    });

    if (!findJogos) {
      return this.converter.toDTO(
        await this.repository.save(this.converter.toEntity(jogos)),
      );
    } else {
      throw new BadRequestException('Jogo já Cadastrado!');
    }
  }

  async update(jogos: JogosDTO): Promise<UpdateResult> {
    const dto = await this.findOneByID(jogos.id);

    dto.descricao = jogos.descricao;
    dto.idCategoria =
      typeof jogos.idCategoria === undefined || jogos.idCategoria != 0
        ? jogos.idCategoria
        : dto.idCategoria;

    return await this.repository.update(dto.id, this.converter.toEntity(dto));
  }

  async delete(id: number): Promise<DeleteResult> {
    await this.findOneByID(id);

    return await this.repository.delete(id);
  }

  async findAndCount(): Promise<[JogosDTO[], number]> {
    const findJogos = await this.repository.findAndCount();
    return this.converter.toDTOsAndCount(findJogos);
  }

  async findOneByID(id: number): Promise<JogosDTO> {
    const findJogos = await this.repository.findOne({ where: { id: id } });

    if (!findJogos) {
      throw new NotFoundException('Jogo não Encontrado!');
    } else {
      const dto = this.converter.toDTO(findJogos);
      return dto;
    }
  }
}
