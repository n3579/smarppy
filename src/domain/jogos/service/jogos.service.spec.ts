import { BadRequestException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { Categoria } from '../../categoria/entity/categoria.entity';
import { CategoriaService } from '../../categoria/service/categoria.service';
import { JogosConverter } from '../converter/jogos.converter';
import { JogosDTO } from '../dto/jogosDTO.dto';
import { Jogos } from '../entity/jogos.entity';
import { JogosRepository } from '../repository/jogos.repository';
import { JogosService } from './jogos.service';

describe('Testando os metodos da JogosService', () => {
  let service: JogosService;

  const repositoryMock = {
    findOne: jest.fn(),
    save: jest.fn(() => jogosMock),
    update: jest.fn(() => updateResultMock),
    delete: jest.fn(() => updateResultMock),
    findAndCount: jest.fn(() => [[jogosMock], 1]),
  };

  const categoriaServiceMock = {
    findOneByID: jest.fn(),
  };

  const categoriaMock: Categoria = {
    id: 1,
    descricao: 'testeCategoria',
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01T00:00:00-03:00'),
  };

  const jogosMock: Jogos = {
    id: 1,
    descricao: 'teste',
    categoria: categoriaMock,
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01:00:00-03:00'),
  };

  const updateResultMock = {
    affected: 1,
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        JogosService,
        JogosConverter,
        { provide: JogosRepository, useValue: repositoryMock },
        { provide: CategoriaService, useValue: categoriaServiceMock },
      ],
    }).compile();

    service = moduleRef.get(JogosService);
  });

  afterEach(() => {
    repositoryMock.findOne.mockClear();
    repositoryMock.save.mockClear();
    repositoryMock.update.mockClear();
    repositoryMock.delete.mockClear();
    repositoryMock.findAndCount.mockClear();
    categoriaServiceMock.findOneByID.mockClear();
  });

  it('Service está definido', () => {
    expect(service).toBeDefined();
  });

  it('create() - deveria salvar um Jogo', async () => {
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = 'teste';
    entity.idCategoria = 1;
    (entity.dataCriacao = new Date('2022-05-01:00:00-03:00')),
      (entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00'));

    const resp = await service.create(entity);

    expect(resp.id).toBe(1);
    expect(resp.descricao).toEqual('teste');
    expect(resp.dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp.dataAtualizacao).toEqual(new Date('2022-05-01:00:00-03:00'));
  });

  it('create() - deveria retornar uma exceção de Descrição não pode ser vazia', async () => {
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = '';
    entity.idCategoria = 1;
    (entity.dataCriacao = new Date('2022-05-01:00:00-03:00')),
      (entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00'));

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('Descrição não pode ser vazia!'),
    );
  });

  it('create() - deveria retornar uma exceção de ID não pode se zero', async () => {
    const entity = new JogosDTO();
    entity.id = 0;
    entity.descricao = 'teste';
    entity.idCategoria = 1;
    (entity.dataCriacao = new Date('2022-05-01:00:00-03:00')),
      (entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00'));

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('ID não pode se zero!'),
    );
  });

  it('create() - deveria retornar uma exceção de Jogo já Cadastrado', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = 'teste';
    entity.idCategoria = 1;
    entity.dataCriacao = new Date('2022-05-01:00:00-03:00');
    entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00');

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('Jogo já Cadastrado!'),
    );
  });

  it('update() - deveria atualizar o registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jogosMock);
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = 'teste';
    entity.idCategoria = 1;
    entity.dataCriacao = new Date('2022-05-01:00:00-03:00');
    entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00');

    const resp = await service.update(entity);

    expect(resp.affected).toBe(1);
  });

  it('update() - deveria atualizar o registro com o if ternario', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jogosMock);
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = 'teste';
    entity.idCategoria = 0;
    entity.dataCriacao = new Date('2022-05-01:00:00-03:00');
    entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00');

    const resp = await service.update(entity);

    expect(resp.affected).toBe(1);
  });

  it('update() - deveria retornar uma exceção de Jogo não Encontrado', async () => {
    repositoryMock.findOne.mockReturnValueOnce(null);
    const entity = new JogosDTO();
    entity.id = 1;
    entity.descricao = 'teste';
    entity.idCategoria = 1;
    entity.dataCriacao = new Date('2022-05-01:00:00-03:00');
    entity.dataAtualizacao = new Date('2022-05-01:00:00-03:00');

    expect(service.update(entity)).rejects.toThrow(
      new BadRequestException('Jogo não Encontrado!'),
    );
  });

  it('delete() - deveria deletar um registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jogosMock);
    const resp = await service.delete(1);
    expect(resp.affected).toBe(1);
  });

  it('delete() - deveria retornar uma exceção de Categoria não Cadastrada', async () => {
    expect(service.delete(1)).rejects.toThrow(
      new BadRequestException('Jogo não Encontrado!'),
    );
  });

  it('findAndCount() - deveria Buscar uma lista e contar quantos registros foram trazidos', async () => {
    const resp = await service.findAndCount();

    expect(resp[0][0].id).toBe(1);
    expect(resp[0][0].descricao).toEqual('teste');
    expect(resp[0][0].idCategoria).toBe(1);
    expect(resp[0][0].dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp[0][0].dataAtualizacao).toEqual(
      new Date('2022-05-01:00:00-03:00'),
    );
    expect(resp[1]).toBe(1);
  });
});
