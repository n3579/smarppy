import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoriaModule } from '../categoria/caregoria.module';
import { JogosConverter } from './converter/jogos.converter';
import { JogosRepository } from './repository/jogos.repository';
import { JogosService } from './service/jogos.service';

@Module({
  imports: [TypeOrmModule.forFeature([JogosRepository]), CategoriaModule],
  providers: [JogosService, JogosConverter],
  exports: [JogosService],
})
export class JogosModule {}
