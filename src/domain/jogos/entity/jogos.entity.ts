import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Categoria } from '../../categoria/entity/categoria.entity';

@Entity({ name: 'jogos' })
@Index(['id', 'categoria'], { unique: true })
export class Jogos {
  @PrimaryGeneratedColumn()
  id: number;

  @Index({ unique: true })
  @Column({ name: 'descricao' })
  descricao: string;

  @ManyToOne(() => Categoria, (categoria) => categoria.id, {
    nullable: false,
    eager: true,
  })
  @JoinColumn()
  categoria: Categoria;

  @Column({ name: 'createddate' })
  @CreateDateColumn()
  createdDate: Date;

  @Column({ name: 'updatedate' })
  @UpdateDateColumn()
  updateDate: Date;
}
