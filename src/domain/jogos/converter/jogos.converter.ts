import { Categoria } from '../../categoria/entity/categoria.entity';
import { JogosDTO } from '../dto/jogosDTO.dto';
import { Jogos } from '../entity/jogos.entity';

export class JogosConverter {
  toEntity(dto: JogosDTO): Jogos {
    const entity = new Jogos();
    const cateogira = new Categoria();

    entity.id = dto.id;
    entity.descricao = dto.descricao;
    cateogira.id = dto.idCategoria;
    entity.categoria = cateogira;

    return entity;
  }

  toDTO(entity: Jogos): JogosDTO {
    const dto = new JogosDTO();

    dto.id = entity.id;
    dto.descricao = entity.descricao;
    dto.dataCriacao = entity.createdDate;
    dto.dataAtualizacao = entity.updateDate;
    dto.idCategoria = entity.categoria.id;

    return dto;
  }

  toDTOsAndCount(entities: [Jogos[], number]): [JogosDTO[], number] {
    return [this.entityToDTO(entities[0]), entities[1]];
  }

  entityToDTO(entities: Jogos[]) {
    return entities.map((entity) => this.toDTO(entity));
  }
}
