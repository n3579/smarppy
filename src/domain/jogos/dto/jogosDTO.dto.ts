import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class JogosDTO {
  id: number;

  @ApiProperty()
  descricao: string;

  dataCriacao: Date;

  dataAtualizacao: Date;

  @ApiPropertyOptional()
  idCategoria: number;
}
