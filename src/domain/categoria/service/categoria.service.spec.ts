import { BadRequestException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { CategoriaConverter } from '../converter/categoria.converter';
import { CategoriaDTO } from '../dto/categoriaDTO.dto';
import { CategoriaRepository } from '../repository/categoria.repository';
import { CategoriaService } from './categoria.service';

describe('Testando os metodos da CategoriaService', () => {
  let service: CategoriaService;

  const repositoryMock = {
    findOne: jest.fn(),
    save: jest.fn(() => categoriaMock),
    update: jest.fn(() => updateResultMock),
    delete: jest.fn(() => updateResultMock),
    findAndCount: jest.fn(() => [[categoriaMock], 1]),
  };

  const categoriaMock = {
    id: 1,
    descricao: 'teste',
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01T00:00:00-03:00'),
  };

  const updateResultMock = {
    affected: 1,
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        CategoriaService,
        CategoriaConverter,
        { provide: CategoriaRepository, useValue: repositoryMock },
      ],
    }).compile();

    service = moduleRef.get(CategoriaService);
  });

  afterEach(() => {
    repositoryMock.findOne.mockClear();
    repositoryMock.save.mockClear();
    repositoryMock.update.mockClear();
    repositoryMock.delete.mockClear();
    repositoryMock.findAndCount.mockClear();
  });

  it('Service está definido', () => {
    expect(service).toBeDefined();
  });

  it('create() - deveria salvar uma Cagetoria', async () => {
    const entity = new CategoriaDTO();
    entity.descricao = 'teste';

    const resp = await service.create(entity);

    expect(resp.id).toBe(1);
    expect(resp.descricao).toEqual('teste');
    expect(resp.dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp.dataAtualizacao).toEqual(new Date('2022-05-01:00:00-03:00'));
  });

  it('create() - deveria retornar uma exceção de Categoria Já Cadastrada', async () => {
    repositoryMock.findOne.mockReturnValueOnce(categoriaMock);
    const entity = new CategoriaDTO();
    entity.descricao = 'teste';

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('Categoria já Cadastrada!'),
    );
  });

  it('create() - deveria retornar uma exceção de Id não pode ser zero', async () => {
    const entity = new CategoriaDTO();
    entity.id = 0;
    entity.descricao = 'teste';

    expect(service.create(entity)).rejects.toThrowError(
      new BadRequestException('ID não pode se zero!'),
    );
  });

  it('create() - deveria retornar uma exceção de Descrição não pode ser vazia!', async () => {
    const entity = new CategoriaDTO();
    entity.descricao = '';

    expect(service.create(entity)).rejects.toThrowError(
      new BadRequestException('Descrição não pode ser vazia!'),
    );
  });

  it('update() - deveria atualizar o registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const entity = new CategoriaDTO();
    entity.id = 1;
    entity.descricao = 'editado';

    const resp = await service.update(entity);

    expect(resp.affected).toBe(1);
  });

  it('update() - deveria retornar uma exceção de Categoria não Cadastrada', async () => {
    const entity = new CategoriaDTO();
    entity.id = 1;
    entity.descricao = 'editado';

    expect(service.update(entity)).rejects.toThrow(
      new BadRequestException('Categoria não Encontrada!'),
    );
  });

  it('delete() - deveria deletar um registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const resp = await service.delete(1);
    expect(resp.affected).toBe(1);
  });

  it('delete() - deveria retornar uma exceção de Categoria não Cadastrada', async () => {
    expect(service.delete(1)).rejects.toThrow(
      new BadRequestException('Categoria não Encontrada!'),
    );
  });

  it('findAndCount() - deveria Buscar uma lista e contar quantos registros foram trazidos', async () => {
    const resp = await service.findAndCount();

    expect(resp[0][0].id).toBe(1);
    expect(resp[0][0].descricao).toEqual('teste');
    expect(resp[0][0].dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp[0][0].dataAtualizacao).toEqual(
      new Date('2022-05-01:00:00-03:00'),
    );
    expect(resp[1]).toBe(1);
  });
});
