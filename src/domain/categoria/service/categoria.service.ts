import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CategoriaConverter } from '../converter/categoria.converter';
import { CategoriaDTO } from '../dto/categoriaDTO.dto';
import { CategoriaRepository } from '../repository/categoria.repository';

@Injectable()
export class CategoriaService {
  constructor(
    @InjectRepository(CategoriaRepository)
    private repository: CategoriaRepository,
    private converter: CategoriaConverter,
  ) {}

  async create(categoria: CategoriaDTO): Promise<CategoriaDTO> {
    if (categoria.descricao.length === 0) {
      throw new BadRequestException('Descrição não pode ser vazia!');
    }

    if (categoria.id === 0) {
      throw new BadRequestException('ID não pode se zero!');
    }

    const findCategoria = await this.repository.findOne({
      where: { descricao: categoria.descricao },
    });

    if (!findCategoria) {
      const saved = await this.repository.save(categoria);
      return this.converter.toDTO(saved);
    } else {
      throw new BadRequestException('Categoria já Cadastrada!');
    }
  }

  async update(categoria: CategoriaDTO): Promise<UpdateResult> {
    await this.findOneByID(categoria.id);

    return await this.repository.update(
      categoria.id,
      this.converter.toEntity(categoria),
    );
  }

  async delete(id: number): Promise<DeleteResult> {
    await this.findOneByID(id);

    return await this.repository.delete(id);
  }

  async findAndCount(): Promise<[CategoriaDTO[], number]> {
    const findCategoria = await this.repository.findAndCount();

    return this.converter.toDTOsAndCount(findCategoria);
  }

  async findOneByID(id: number): Promise<CategoriaDTO> {
    const findCategoria = await this.repository.findOne({ where: { id: id } });

    if (!findCategoria) {
      throw new NotFoundException('Categoria não Encontrada!');
    } else {
      const dto = this.converter.toDTO(findCategoria);
      return dto;
    }
  }
}
