import { CategoriaDTO } from '../dto/categoriaDTO.dto';
import { Categoria } from '../entity/categoria.entity';

export class CategoriaConverter {
  toEntity(dto: CategoriaDTO): Categoria {
    const entity = new Categoria();

    entity.id = dto.id;
    entity.descricao = dto.descricao;

    return entity;
  }

  toDTO(entity: Categoria): CategoriaDTO {
    const dto = new CategoriaDTO();

    dto.id = entity.id;
    dto.descricao = entity.descricao;
    dto.dataCriacao = entity.createdDate;
    dto.dataAtualizacao = entity.updateDate;

    return dto;
  }

  toDTOsAndCount(entities: [Categoria[], number]): [CategoriaDTO[], number] {
    return [this.entityToDTO(entities[0]), entities[1]];
  }

  entityToDTO(entities: Categoria[]) {
    return entities.map((entity) => this.toDTO(entity));
  }
}
