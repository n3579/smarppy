import { EntityRepository, Repository } from 'typeorm';
import { Categoria } from '../entity/categoria.entity';

@EntityRepository(Categoria)
export class CategoriaRepository extends Repository<Categoria> {}
