import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoriaConverter } from './converter/categoria.converter';
import { CategoriaRepository } from './repository/categoria.repository';
import { CategoriaService } from './service/categoria.service';

@Module({
  imports: [TypeOrmModule.forFeature([CategoriaRepository])],
  providers: [CategoriaService, CategoriaConverter],
  exports: [CategoriaService],
})
export class CategoriaModule {}
