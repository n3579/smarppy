import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'categoria' })
export class Categoria {
  @PrimaryGeneratedColumn()
  id: number;

  @Index({ unique: true })
  @Column({ name: 'descricao' })
  descricao: string;

  @Column({ name: 'createddate' })
  @CreateDateColumn()
  createdDate: Date;

  @Column({ name: 'updatedate' })
  @UpdateDateColumn()
  updateDate: Date;
}
