import { ApiProperty } from '@nestjs/swagger';

export class CategoriaDTO {
  id: number;

  @ApiProperty()
  descricao: string;

  dataCriacao: Date;

  dataAtualizacao: Date;
}
