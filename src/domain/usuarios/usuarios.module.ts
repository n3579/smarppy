import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuariosConverter } from './converter/usuarios.converter';
import { UsuariosRepository } from './repository/usuarios.repository';
import { UsuariosService } from './service/usuarios.service';

@Module({
  imports: [TypeOrmModule.forFeature([UsuariosRepository])],
  providers: [UsuariosService, UsuariosConverter],
  exports: [UsuariosService],
})
export class UsuariosModule {}
