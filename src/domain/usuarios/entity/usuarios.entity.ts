import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'usuarios' })
export class Usuarios {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'email' })
  @Index({ unique: true })
  email: string;

  @Column({ name: 'senha' })
  senha: string;

  @Column({ name: 'createddate' })
  @CreateDateColumn()
  createdDate: Date;

  @Column({ name: 'updatedate' })
  @UpdateDateColumn()
  updateDate: Date;
}
