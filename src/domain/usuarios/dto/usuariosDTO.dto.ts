import { ApiProperty } from '@nestjs/swagger';

export class UsuariosDTO {
  id: number;

  @ApiProperty()
  email: string;

  @ApiProperty()
  senha: string;

  dataCriacao: Date;

  dataAtualizacao: Date;
}
