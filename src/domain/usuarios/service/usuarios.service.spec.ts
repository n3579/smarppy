import { BadRequestException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { UsuariosConverter } from '../converter/usuarios.converter';
import { UsuariosDTO } from '../dto/usuariosDTO.dto';
import { Usuarios } from '../entity/usuarios.entity';
import { UsuariosRepository } from '../repository/usuarios.repository';
import { UsuariosService } from './usuarios.service';

describe('Testando os metodos da UsuariosService', () => {
  let service: UsuariosService;

  const repositoryMock = {
    findOne: jest.fn(),
    save: jest.fn(() => usuariosMock),
    update: jest.fn(() => updateResultMock),
    delete: jest.fn(() => updateResultMock),
    findAndCount: jest.fn(() => [[usuariosMock], 1]),
  };

  const usuariosMock: Usuarios = {
    id: 1,
    email: 'teste',
    senha: '123',
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01:00:00-03:00'),
  };

  const updateResultMock = {
    affected: 1,
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UsuariosService,
        UsuariosConverter,
        { provide: UsuariosRepository, useValue: repositoryMock },
      ],
    }).compile();

    service = moduleRef.get(UsuariosService);
  });

  afterEach(() => {
    repositoryMock.findOne.mockClear();
    repositoryMock.save.mockClear();
    repositoryMock.update.mockClear();
    repositoryMock.delete.mockClear();
    repositoryMock.findAndCount.mockClear();
  });

  it('Service está definido', () => {
    expect(service).toBeDefined();
  });

  it('create() - deveria salvar um Usuario', async () => {
    const entity = new UsuariosDTO();
    entity.email = 'teste';
    entity.senha = '123';

    const resp = await service.create(entity);

    expect(resp.id).toBe(1);
    expect(resp.email).toEqual('teste');
    expect(resp.dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp.dataAtualizacao).toEqual(new Date('2022-05-01:00:00-03:00'));
  });

  it('create() - deveria retornar uma exceção de Usuario já Cadastrado!', async () => {
    repositoryMock.findOne.mockReturnValueOnce(usuariosMock);
    const entity = new UsuariosDTO();
    entity.email = 'teste';
    entity.senha = '123';

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('Usuario já Cadastrado!'),
    );
  });

  it('create() - deveria retornar uma exceção de Email não pode ser vazio!', async () => {
    const entity = new UsuariosDTO();
    entity.email = '';
    entity.senha = '123';

    expect(service.create(entity)).rejects.toThrowError(
      new BadRequestException('Email não pode ser vazio!'),
    );
  });

  it('create() - deveria retornar uma exceção de Senha não pode ser vazia!', async () => {
    const entity = new UsuariosDTO();
    entity.email = '123';
    entity.senha = '';

    expect(service.create(entity)).rejects.toThrowError(
      new BadRequestException('Senha não pode ser vazia!'),
    );
  });

  it('update() - deveria atualizar o registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const entity = new UsuariosDTO();
    entity.id = 1;
    entity.email = '123';
    entity.senha = '';

    const resp = await service.update(entity);

    expect(resp.affected).toBe(1);
  });

  it('update() - deveria atualizar o registro utilizando o if ternario', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const entity = new UsuariosDTO();
    entity.id = 1;
    entity.email = '123';
    entity.senha = '321';

    const resp = await service.update(entity);

    expect(resp.affected).toBe(1);
  });

  it('update() - deveria retornar uma exceção de Usuario não Encontrado', async () => {
    const entity = new UsuariosDTO();
    entity.id = 1;
    entity.email = '123';
    entity.senha = '';

    expect(service.update(entity)).rejects.toThrow(
      new BadRequestException('Usuario não Encontrado!'),
    );
  });

  it('delete() - deveria deletar um registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const resp = await service.delete(1);
    expect(resp.affected).toBe(1);
  });

  it('delete() - deveria retornar uma exceção de Usuario não Encontrado', async () => {
    expect(service.delete(1)).rejects.toThrow(
      new BadRequestException('Usuario não Encontrado!'),
    );
  });

  it('findAndCount() - deveria Buscar uma lista e contar quantos registros foram trazidos', async () => {
    const resp = await service.findAndCount();

    expect(resp[0][0].id).toBe(1);
    expect(resp[0][0].email).toEqual('teste');
    expect(resp[0][0].dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp[0][0].dataAtualizacao).toEqual(
      new Date('2022-05-01:00:00-03:00'),
    );
    expect(resp[1]).toBe(1);
  });

  it('findOneByEmail() - deveria Buscar um registro pelo email', async () => {
    repositoryMock.findOne.mockReturnValueOnce(usuariosMock);

    const resp = await service.findOneByEmail('teste');

    expect(resp.id).toBe(1);
    expect(resp.email).toEqual('teste');
    expect(resp.senha).toEqual('123');
    expect(resp.dataCriacao).toEqual(new Date('2022-05-01:00:00-03:00'));
    expect(resp.dataAtualizacao).toEqual(new Date('2022-05-01:00:00-03:00'));
  });

  it('findOneByEmail() - deveria retornar uma exceção de Usuario não Encontrado', async () => {
    expect(service.findOneByEmail('')).rejects.toThrow(
      new BadRequestException('Usuario não Encontrado!'),
    );
  });
});
