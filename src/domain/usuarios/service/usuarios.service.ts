import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, UpdateResult } from 'typeorm';
import { UsuariosConverter } from '../converter/usuarios.converter';
import { UsuariosDTO } from '../dto/usuariosDTO.dto';
import { UsuariosRepository } from '../repository/usuarios.repository';

@Injectable()
export class UsuariosService {
  constructor(
    @InjectRepository(UsuariosRepository)
    private repository: UsuariosRepository,
    private converter: UsuariosConverter,
  ) {}

  async create(usuarios: UsuariosDTO): Promise<UsuariosDTO> {
    if (usuarios.email.length === 0) {
      throw new BadRequestException('Email não pode ser vazio!');
    }

    if (usuarios.senha.length === 0) {
      throw new BadRequestException('Senha não pode ser vazia!');
    }

    const findUsuarios = await this.repository.findOne({
      where: { email: usuarios.email },
    });

    if (!findUsuarios) {
      return this.converter.toDTO(
        await this.repository.save(this.converter.toEntity(usuarios)),
      );
    } else {
      throw new BadRequestException('Usuario já Cadastrado!');
    }
  }

  async update(usuarios: UsuariosDTO): Promise<UpdateResult> {
    const dto = await this.findOneByID(usuarios.id);
    dto.email = usuarios.email;
    dto.senha =
      usuarios.senha === '' || usuarios.senha.length === 0
        ? dto.email
        : usuarios.senha;

    return await this.repository.update(dto.id, this.converter.toEntity(dto));
  }

  async delete(id: number): Promise<DeleteResult> {
    await this.findOneByID(id);

    return await this.repository.delete(id);
  }

  async findAndCount(): Promise<[UsuariosDTO[], number]> {
    const findUsuarios = await this.repository.findAndCount();
    return this.converter.toDTOsAndCount(findUsuarios);
  }

  async findOneByID(id: number): Promise<UsuariosDTO> {
    const findUsuarios = await this.repository.findOne({ where: { id: id } });

    if (!findUsuarios) {
      throw new NotFoundException('Usuario não Encontrado!');
    } else {
      const dto = this.converter.toDTO(findUsuarios);
      return dto;
    }
  }

  async findOneByEmail(email: string): Promise<UsuariosDTO> {
    const findUsuarios = await this.repository.findOne({
      where: { email: email },
    });

    if (!findUsuarios) {
      throw new NotFoundException('Usuario não Encontrado!');
    } else {
      const dto = this.converter.toDTO(findUsuarios);
      dto.senha = findUsuarios.senha;
      return dto;
    }
  }
}
