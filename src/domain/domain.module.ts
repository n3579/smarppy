import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { CategoriaModule } from './categoria/caregoria.module';
import { JogosModule } from './jogos/jogos.module';
import { UsuariosModule } from './usuarios/usuarios.module';
import { UsuariosJogosModule } from './usuariosjogos/usuariosjogos.module';

@Module({
  imports: [
    CategoriaModule,
    JogosModule,
    UsuariosModule,
    UsuariosJogosModule,
    AuthModule,
  ],
  exports: [
    CategoriaModule,
    JogosModule,
    UsuariosModule,
    UsuariosJogosModule,
    AuthModule,
  ],
})
export class DomainModule {}
