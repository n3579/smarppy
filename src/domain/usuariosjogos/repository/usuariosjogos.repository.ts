import { EntityRepository, Repository } from 'typeorm';
import { UsuariosJogos } from '../entity/usuariosjogos.entity';

@EntityRepository(UsuariosJogos)
export class UsuariosJogosRepository extends Repository<UsuariosJogos> {
  listarJogosQuantidade(limit?: boolean) {
    let sql =
      'select j.descricao as Jogo, count(uj.jogosId) as Quantidade' +
      '  from usuariosjogos uj' +
      '  join jogos j ' +
      '    on j.id = uj.jogosId ' +
      'group by j.descricao,' +
      '         uj.jogosId ' +
      'order by count(uj.jogosId) desc';

    sql += limit === true ? ' limit 5' : '';

    return this.query(sql);
  }
}
