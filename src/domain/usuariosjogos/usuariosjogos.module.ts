import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JogosModule } from '../jogos/jogos.module';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { UsuariosJogosConverter } from './converter/usuariosjogos.converter';
import { UsuariosJogosRepository } from './repository/usuariosjogos.repository';
import { UsuariosJogosService } from './service/usuariosjogos.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([UsuariosJogosRepository]),
    UsuariosModule,
    JogosModule,
  ],
  providers: [UsuariosJogosService, UsuariosJogosConverter],
  exports: [UsuariosJogosService],
})
export class UsuariosJogosModule {}
