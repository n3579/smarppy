import {
  Entity,
  Index,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Jogos } from '../../jogos/entity/jogos.entity';
import { Usuarios } from '../../usuarios/entity/usuarios.entity';

@Entity({ name: 'usuariosjogos' })
@Index(['jogos', 'usuarios'], { unique: true })
export class UsuariosJogos {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Jogos, (jogos) => jogos.id, { nullable: false, eager: true })
  @JoinTable()
  jogos: Jogos;

  @ManyToOne(() => Usuarios, (usuarios) => usuarios.id, {
    nullable: false,
    eager: true,
  })
  @JoinTable()
  usuarios: Usuarios;
}
