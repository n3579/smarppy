import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult } from 'typeorm';
import { ReadStream } from 'typeorm/platform/PlatformTools';
import { JogosService } from '../../jogos/service/jogos.service';
import { UsuariosService } from '../../usuarios/service/usuarios.service';
import { UsuariosJogosConverter } from '../converter/usuariosjogos.converter';
import { UsuariosJogosDTO } from '../dto/usuariosjogos.dto';
import { UsuariosJogosRepository } from '../repository/usuariosjogos.repository';

@Injectable()
export class UsuariosJogosService {
  constructor(
    @InjectRepository(UsuariosJogosRepository)
    private repository: UsuariosJogosRepository,
    private usuariosService: UsuariosService,
    private jogosService: JogosService,
    private converter: UsuariosJogosConverter,
  ) {}

  async create(dto: UsuariosJogosDTO): Promise<UsuariosJogosDTO> {
    await this.usuariosService.findOneByID(dto.idUsuario);
    await this.jogosService.findOneByID(dto.idJogo);

    const entity = await this.repository.find({
      where: { jogos: { id: dto.idJogo }, usuarios: { id: dto.idUsuario } },
    });

    if (!entity) {
      return this.converter.toDTO(
        await this.repository.save(this.converter.toEntity(dto)),
      );
    } else {
      throw new BadRequestException(
        'Este jogo já está vinculado a este usuario!',
      );
    }
  }

  async delete(id: number): Promise<DeleteResult> {
    const entity = await this.repository.findOne({ where: { id: id } });

    if (!entity) {
      throw new NotFoundException('Vinculo não encontrado!');
    } else {
      return await this.repository.delete(id);
    }
  }

  async findAndCount(): Promise<[UsuariosJogosDTO[], number]> {
    const findUsuariosJogos = await this.repository.findAndCount();
    return this.converter.toDTOsAndCount(findUsuariosJogos);
  }

  async findOneByID(id: number): Promise<UsuariosJogosDTO> {
    const findUsuariosJogos = await this.repository.findOne({
      where: { id: id },
    });

    if (!findUsuariosJogos) {
      throw new NotFoundException('Vinculo não Encontrado!');
    } else {
      const dto = this.converter.toDTO(findUsuariosJogos);
      return dto;
    }
  }

  async listarJogosQuantidade(limit?: boolean): Promise<ReadStream> {
    return this.repository.listarJogosQuantidade(limit);
  }
}
