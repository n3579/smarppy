import { BadRequestException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { Categoria } from '../../categoria/entity/categoria.entity';
import { Jogos } from '../../jogos/entity/jogos.entity';
import { JogosService } from '../../jogos/service/jogos.service';
import { Usuarios } from '../../usuarios/entity/usuarios.entity';
import { UsuariosService } from '../../usuarios/service/usuarios.service';
import { UsuariosJogosConverter } from '../converter/usuariosjogos.converter';
import { UsuariosJogosDTO } from '../dto/usuariosjogos.dto';
import { UsuariosJogos } from '../entity/usuariosjogos.entity';
import { UsuariosJogosRepository } from '../repository/usuariosjogos.repository';
import { UsuariosJogosService } from './usuariosjogos.service';

describe('Testando os metodos da UsuariosJogosService', () => {
  let service: UsuariosJogosService;

  const repositoryMock = {
    findOne: jest.fn(),
    save: jest.fn(() => usuariosJogosMock),
    delete: jest.fn(() => updateResultMock),
    findAndCount: jest.fn(() => [[usuariosJogosMock], 1]),
    find: jest.fn(),
  };

  const updateResultMock = {
    affected: 1,
  };

  const categoriaMock: Categoria = {
    id: 1,
    descricao: 'testeCategoria',
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01T00:00:00-03:00'),
  };

  const jogosMock: Jogos = {
    id: 1,
    descricao: 'teste',
    categoria: categoriaMock,
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01:00:00-03:00'),
  };

  const usuariosMock: Usuarios = {
    id: 1,
    email: 'teste',
    senha: '123',
    createdDate: new Date('2022-05-01:00:00-03:00'),
    updateDate: new Date('2022-05-01:00:00-03:00'),
  };

  const usuariosJogosMock: UsuariosJogos = {
    id: 1,
    jogos: jogosMock,
    usuarios: usuariosMock,
  };

  const jogosServiceMock = {
    findOneByID: jest.fn(),
  };

  const usuariosServiceMock = {
    findOneByID: jest.fn(),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UsuariosJogosService,
        UsuariosJogosConverter,
        { provide: UsuariosJogosRepository, useValue: repositoryMock },
        { provide: JogosService, useValue: jogosServiceMock },
        { provide: UsuariosService, useValue: usuariosServiceMock },
      ],
    }).compile();

    service = moduleRef.get(UsuariosJogosService);
  });

  afterEach(() => {
    repositoryMock.findOne.mockClear();
    repositoryMock.save.mockClear();
    repositoryMock.delete.mockClear();
    repositoryMock.findAndCount.mockClear();
    jogosServiceMock.findOneByID.mockClear();
    usuariosServiceMock.findOneByID.mockClear();
  });

  it('Service está definido', () => {
    expect(service).toBeDefined();
  });

  it('create() - deveria salvar um UsuarioJogos', async () => {
    const entity = new UsuariosJogosDTO();
    entity.id = 1;
    entity.idJogo = 1;
    entity.idUsuario = 1;

    const resp = await service.create(entity);

    expect(resp.id).toBe(1);
    expect(resp.idJogo).toBe(1);
    expect(resp.idUsuario).toBe(1);
  });

  it('create() - deveria retornar uma exceção de Este jogo já está vinculado a este usuario', async () => {
    repositoryMock.find.mockReturnValueOnce({});
    const entity = new UsuariosJogosDTO();
    entity.id = 1;
    entity.idJogo = 1;
    entity.idUsuario = 1;

    expect(service.create(entity)).rejects.toThrow(
      new BadRequestException('Este jogo já está vinculado a este usuario!'),
    );
  });

  it('delete() - deveria deletar um registro', async () => {
    repositoryMock.findOne.mockReturnValueOnce(jest.fn());
    const resp = await service.delete(1);
    expect(resp.affected).toBe(1);
  });

  it('delete() - deveria retornar uma exceção de Vinculo não encontrado', async () => {
    expect(service.delete(1)).rejects.toThrow(
      new BadRequestException('Vinculo não encontrado!'),
    );
  });

  it('findAndCount() - deveria Buscar uma lista e contar quantos registros foram trazidos', async () => {
    const resp = await service.findAndCount();

    expect(resp[0][0].id).toBe(1);
    expect(resp[0][0].idJogo).toBe(1);
    expect(resp[0][0].idUsuario).toBe(1);
    expect(resp[1]).toBe(1);
  });

  it('findOneByID() - deveria retornar uma exceção de Vinculo não encontrado', async () => {
    expect(service.findOneByID(1)).rejects.toThrow(
      new BadRequestException('Vinculo não Encontrado!'),
    );
  });

  it('findOneByID() - deveria Buscar uma registro pelo ID', async () => {
    repositoryMock.findOne.mockReturnValueOnce(usuariosJogosMock);
    const resp = await service.findOneByID(1);

    expect(resp.id).toBe(1);
    expect(resp.idJogo).toBe(1);
    expect(resp.idUsuario).toBe(1);
  });
});
