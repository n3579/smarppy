import { Jogos } from '../../jogos/entity/jogos.entity';
import { Usuarios } from '../../usuarios/entity/usuarios.entity';
import { UsuariosJogosDTO } from '../dto/usuariosjogos.dto';
import { UsuariosJogos } from '../entity/usuariosjogos.entity';

export class UsuariosJogosConverter {
  toEntity(dto: UsuariosJogosDTO): UsuariosJogos {
    const entity = new UsuariosJogos();
    const jogos = new Jogos();
    const usuarios = new Usuarios();

    entity.id = dto.id;
    jogos.id = dto.idJogo;
    entity.jogos = jogos;
    usuarios.id = dto.idUsuario;
    entity.usuarios = usuarios;

    return entity;
  }

  toDTO(entity: UsuariosJogos): UsuariosJogosDTO {
    const dto = new UsuariosJogosDTO();

    dto.id = entity.id;
    dto.idJogo = entity.jogos.id;
    dto.idUsuario = entity.usuarios.id;

    return dto;
  }

  toDTOsAndCount(
    entities: [UsuariosJogos[], number],
  ): [UsuariosJogosDTO[], number] {
    return [this.entityToDTO(entities[0]), entities[1]];
  }

  entityToDTO(entities: UsuariosJogos[]) {
    return entities.map((entity) => this.toDTO(entity));
  }
}
