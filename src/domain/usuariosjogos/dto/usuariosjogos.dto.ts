import { ApiProperty } from '@nestjs/swagger';

export class UsuariosJogosDTO {
  id: number;

  @ApiProperty()
  idJogo: number;

  @ApiProperty()
  idUsuario: number;
}
