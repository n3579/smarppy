# Api para Biblioteca de Jogos

## Overview
Esta aplicação foi desenvolvida usando:
 * NestJS
 * TypeORM
 * MySQL 8.0.28 - Community Server - GPL
 * Jest
 * Yarn
 * Documentação com Swagger (Somente vizualização, pois necessita do Token JWT para funcionar)

Durante o desenvolvimento optei por utilizar todas as ferramentas do TypeORM para modelagem, ou seja, não é necessário o uso de scripts para geração das entidades, relações, índices, etc...
Durante a leitura do material para a prova, fiquei com algumas duvidas então tomei a liberdade de alterar e criar algumas rotas, por exemplo:
É dito no teste que é preciso logar com o usuário para gerar o token, porém as rotas de usuários devem ser privadas... então deixei a rota de cadastro como Pública, para o usuário realizar o cadastro e então utilizar o Login para geração do Token.
Também não há na documentação qual a forma de Vínculo entre o Usuário e os Jogos, pensando nisso criei uma rota de Vínculo e Remoção de Vínculo, ou seja, o Usuário Adiciona e Remove o jogo da sua biblioteca por meio de um endpoint.

Vale salientar também de que mesmo não sendo necessário na documentação do teste, está aplicação possui testes unitários.

## Configuração.

Para a configuração do programa é necessário baixar as dependências utilizando os comandos do yarn dentro da pasta da aplicação.

    comando: yarn
Após baixar as dependências, as propriedades do banco deverão ser alteradas no arquivo **.env** na pasta raiz do projeto.

Para rodar a aplicação, entre na pasta do projeto e execute os comandos de acordo com o ambiente desejado:

    desenvolvimento: yarn start
    desenvolvimento com watch: yarn start:dev
    produção: yarn start:prod
Para rodar os teste unitários, também na pasta do projeto, execute os comandos:

    executar os teste: yarn test
    executar os testes com a cobertura: yarn test:cov
Para utilizar o Swagger, entre em seu navegador em https://localhost:3000/swagger (no caso das configurações do .env estarem como padrão)
A Maioria dos endpoins está documentado porém não é possível utilizar seus endpoints devido a obrigatoriedade do TokenJWT.

## Geração do Token para Autenticação

Para a geração do Token, é preciso antes utilizar o endpoind de Cadastro.
Após feito o Cadastro de Usuario, é necessário enviar o seguinte JSON no corpo da requisição **auth/login**

    {
	"email": "string",
	"senha": "string"
	}

Substituindo os values "string" pelos respectivos dados cadastrados.